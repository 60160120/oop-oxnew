
public class Player {
	private String name;
	private int win, draw, lose;

	public Player(String name) {
		this.name = name;
		win = 0;
		draw = 0;
		lose = 0;
	}

	public String getName() {
		return name;
	}

	public int getWin() {
		return win;
	}

	public int getDraw() {
		return draw;
	}

	public int getLose() {
		return lose;
	}

	public void setWin() {
		win++;
	}

	public void setDraw() {
		draw++;
	}

	public void setLose() {
		lose++;
	}

}
