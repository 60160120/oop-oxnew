import java.util.Scanner;

public class Game {
	private Board board;
	private Player x;
	private Player o;
	static Scanner kb = new Scanner(System.in);

	public Game() {
		o = new Player("O");
		x = new Player("X");

	}

	public void play() {
		showWelcome();
		showGoodbye();
	}
	
	private void showWelcome() {
		System.out.println("Welcome to OX Game.\n");
	}
	
	private void showGoodbye() {
		System.out.println("Thank for play.");
	}
	

}
