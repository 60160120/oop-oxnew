import java.util.Scanner;

public class Board {
	private String[][] table = new String[3][3];
	private Player x;
	private Player o;
	private Player winner;
	private Player current;
	private int turnCount;
	static Scanner kb = new Scanner(System.in);;

	public Board(Player x, Player o) {
		this.x = x;
		this.o = o;
		current = x;
		winner = null;
		turnCount = 0;
	}
	
}
